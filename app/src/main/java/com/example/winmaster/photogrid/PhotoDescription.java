package com.example.winmaster.photogrid;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import java.util.Random;

public class PhotoDescription extends AppCompatActivity
{
    public ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_description);

        Random random = new Random();
        imageView = findViewById(R.id.Image);

        /*
            For me, the best way of storing animations in Android and animate elements is to use resource animation
            in 'anim' directory of project, and simple to load them to run

            More simple code, separation of concepts, reusable animations
            Definition of animation is in file: anim/animation_name.xml

            Feel free to adjust animations by yourself if you like :)
        */

        //Very bad programming - but handy for this tutorial :D - switch for make different animation (one of 5)
        switch (random.nextInt(5))
        {
            case 0: //animation rotate
            {
                Animation rotate = AnimationUtils.loadAnimation(this, R.anim.rotate);
                imageView.startAnimation(rotate);
                break;
            }

            case 1: //animation fade and zoom
            {
                Animation fadeAndZoom = AnimationUtils.loadAnimation(this, R.anim.fade_and_zoom);
                imageView.startAnimation(fadeAndZoom);
                break;
            }
            case 2: //animation blink
            {
                Animation blink = AnimationUtils.loadAnimation(this, R.anim.blink);
                imageView.startAnimation(blink);
                break;
            }
            case 3: //animation bounce
            {
                Animation bounce = AnimationUtils.loadAnimation(this, R.anim.bounce);
                imageView.startAnimation(bounce);
                break;
            }
            case 4: //animation slide up
            {
                Animation slideUp = AnimationUtils.loadAnimation(this, R.anim.slide_up);
                imageView.startAnimation(slideUp);
                break;
            }
        }
    }
}
