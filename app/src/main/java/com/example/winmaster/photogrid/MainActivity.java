package com.example.winmaster.photogrid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity
{
    public LayoutInflater layoutInflater;
    public ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        //standard operations for inflate view of Main activity
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //getting layoutInflater to show photo cell in next steps
        layoutInflater = getLayoutInflater();

        //getting gridView photos reference from view file
        GridView gridViewPhotos = findViewById(R.id.gridViewPhotos);

        //adding adapter (BaseAdapter class) to handle image view cell to show in a GridView
        gridViewPhotos.setAdapter(new BaseAdapter()
        {
            //for this example purpose only - do not use strict elements amount on production code!
            @Override
            public int getCount()
            {
                return 20;
            }

            @Override
            public Object getItem(int i)
            {
                return getItemId(i);
            }

            @Override
            public long getItemId(int i)
            {
                return i;
            }

            //return view of photo layout file
            @Override
            public View getView(int i, View view, ViewGroup viewGroup)
            {
                return layoutInflater.inflate(R.layout.photo, viewGroup, false);
            }
        });

        //Handle photo show in new Activity screen on click action
        gridViewPhotos.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                //passing new Intent to start Activity
                startActivity(new Intent(MainActivity.this, PhotoDescription.class));
            }
        });
    }
}
