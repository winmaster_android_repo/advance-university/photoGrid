# Photo Grid Application with simple animations
All animations are stored in 'anim' directory of project.
This solution makes code easier to understand, easier to maintenance, cleaner and reusable

## Sample app
![Sample Video](video/animations.mp4)

## Rotation
app/src/main/res/anim/rotate.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<set xmlns:android="http://schemas.android.com/apk/res/android">
    <rotate android:fromDegrees="0"
        android:toDegrees="360"
        android:pivotX="50%"
        android:pivotY="50%"
        android:duration="700"
        android:repeatMode="restart"
        android:repeatCount="0"
        android:interpolator="@android:anim/cycle_interpolator"/>

</set>

```

## Blink
app/src/main/res/anim/blink.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<set xmlns:android="http://schemas.android.com/apk/res/android">
    <alpha android:fromAlpha="0.0"
        android:toAlpha="1.0"
        android:interpolator="@android:anim/accelerate_interpolator"
        android:duration="600"
        android:repeatMode="reverse"
        android:repeatCount="3"/>
</set>
```

## Bounce
app/src/main/res/anim/bounce.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<set xmlns:android="http://schemas.android.com/apk/res/android"
    android:fillAfter="true"
    android:interpolator="@android:anim/bounce_interpolator">

    <scale
        android:duration="800"
        android:fromXScale="2.0"
        android:fromYScale="0.0"
        android:toXScale="1.0"
        android:toYScale="1.0" />

</set>

```
## Slide up
app/src/main/res/anim/slide_up.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<set xmlns:android="http://schemas.android.com/apk/res/android"
    android:fillAfter="true" >

    <scale
        android:duration="700"
        android:fromXScale="1.0"
        android:fromYScale="0.0"
        android:toXScale="1.0"
        android:toYScale="1.0" />

</set>

```
## Fade and zoom
app/src/main/res/anim/fade_and_zoom.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<set
    xmlns:android="http://schemas.android.com/apk/res/android">
    <alpha
        android:fromAlpha = "0.0"
        android:toAlpha="1.0"
        android:duration="2000" />
    <scale
        android:fromXScale="0.5"
        android:fromYScale="0.5"
        android:pivotX="50%"
        android:pivotY="50%"
        android:toXScale="1"
        android:toYScale="1"
        android:duration="2000"/>
</set>

```